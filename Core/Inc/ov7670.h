#ifndef _OV7660_H
#define _OV7660_H

#include "SCCB.h"
#include "main.h"

#define OV7670_D0           GPIO_PIN_0  //PB0
#define OV7670_D1           GPIO_PIN_1  //PB1
#define OV7670_D2           GPIO_PIN_2  //PB2
#define OV7670_D3           GPIO_PIN_3  //PB3
#define OV7670_D4           GPIO_PIN_4  //PB4
#define OV7670_D5           GPIO_PIN_5  //PB5
#define OV7670_D6           GPIO_PIN_6  //PB6
#define OV7670_D7           GPIO_PIN_7  //PB7

#define OV_VSYNC_BIT       GPIO_PIN_0	 //接口PA0
#define OV_HREF_BIT        GPIO_PIN_1   //接口PA1
#define OV_PCLK_BIT        GPIO_PIN_2	 //接口PA2
#define OV_XCLK_BIT        GPIO_PIN_8   //接口PA8
#define OV7670_RESET_PIN    GPIO_PIN_15	 //接口PC15

#define OV_VSYNC_STATE      (GPIOA->IDR & 0x00000001)   //HAL_GPIO_ReadPin(GPIOA, LCD_VSYNC_BIT)
#define OV_HREF_STATE       (GPIOA->IDR & 0x00000002)   //HAL_GPIO_ReadPin(GPIOA, LCD_HREF_BIT)
#define OV_PCLK_STATE       (GPIOA->IDR & 0x00000004)   //HAL_GPIO_ReadPin(GPIOA, LCD_PCLK_BIT)

#define Camera_SET          HAL_GPIO_WritePin(GPIOC, OV7670_RESET_PIN, GPIO_PIN_SET)    //PC15
#define Camera_RESET        HAL_GPIO_WritePin(GPIOC, OV7670_RESET_PIN, GPIO_PIN_RESET)  //PC15

/////////////////////////////////////////
void CLK_init_ON(void);
void CLK_init_OFF(void);
unsigned char wrOV7660Reg(unsigned char regID, unsigned char regDat);
unsigned char rdOV7660Reg(unsigned char regID, unsigned char *regDat);
void OV7660_config_window(unsigned int startx,unsigned int starty,unsigned int width, unsigned int height);
unsigned char OV7660_init(void);
unsigned char OV7670_init(void);
//void ov7660_GPIO_CONTRL_CONFIG(void)
void Init_XCLK(void);

extern TIM_HandleTypeDef htim1;

#endif /* _OV7660_H */



