/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "crc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "delay.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd.h"
#include "stdio.h"
#include "OV7670.h"
#include "SCCB.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
static uint8_t camera_dada[40960]={0,};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#define BUFFER_SIZE    5
static const uint32_t dataBuffer[BUFFER_SIZE] =
{
    0x00001021,0x00001021,0x00001021,0x00001021,0x00001021
};
uint32_t uwExpectedCRCValue = 0xEAFC44FB;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#define POLY 0x04C11DB7

uint32_t CrcGen_STM32(uint32_t * data, uint32_t size)
{
	uint32_t i,j,temp,crc;
	crc = 0xFFFFFFFF;                      //init crc value;
	for(i=0; i<size; i++)
	{
		temp = data[i];
		for(j=0; j<32; j++)
		{
      if((crc ^ temp) & 0x80000000 )
      {
			 crc = (crc<<1) ^ POLY ;
			}
			else
			{
				crc <<=1;
			}
				temp<<=1;
     }
	}
		return crc;
}

uint32_t STM32_CRC_Cal(uint32_t * data, uint32_t size)
{
	uint32_t i,j,temp,crc;
	crc = 0xFFFFFFFF;                      //init crc value;
	for(i=0; i<size; i++)
	{
		temp = data[i];
		crc= crc ^ temp;
		for(j=0; j<32; j++)
		{
      if(crc & 0x80000000 )
      {
			  crc = (crc<<1) ^ 0x04C11DB7 ;
			}
			else
			{
				crc <<=1;
			}				
     }
	}
		return crc;
}

void xianshi()//显示信息
{ 
	BACK_COLOR=WHITE;
	POINT_COLOR=BLUE;   
	//显示32*32汉字
	showhanzi32(0,0,0);	    //淘
	showhanzi32(40,0,1);	  //晶
	showhanzi32(80,0,2);    //驰
	//显示16*16汉字
	showhanzi16(0,35,0);	  //专
	showhanzi16(20,35,1);	  //注
	showhanzi16(40,35,2);	  //显
	showhanzi16(60,35,3);	  //示
	showhanzi16(80,35,4);	  //方
	showhanzi16(100,35,5);	//案	   
	LCD_ShowString(0,55,200,16,16,"1.8 TFT SPI DIS");
}

void showqq()
{ 
	u16 x,y; 
	x=0;
	y=75;
	while(y<lcddev.height-39)
	{
		x=0;
		while(x<lcddev.width-39)
		{
			showimage(x,y);	
			x+=40;
		}
		y+=40;
	 }	  
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  __IO uint32_t CRCValue = 0;		 
	uint16_t p_data;
  uint8_t data_temp;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CRC_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
	
    printf("start\n");
	//while(1);
	LCD_Init();	  			
	xianshi();	       //显示信息
	showqq();	         //显示QQ	
	//show_128x160();
	//TCS34725_Init();
	//HAL_Delay(5000);
	CLK_init_ON();
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);

	while( OV7670_init() != 1);	
	printf("OV init finished\r\n");

  LCD_SetCursor(0,0);               //set the 0,0 origin
	LCD_WriteRAM_Prepare();
  //HAL_Delay(2000);
	while(1)                           //DataCnt < 40960)//128x160 pixels;
  {
    static uint32_t i,j,k,m,n;
 /*captuer a frame data*/
    k=0;
    while(OV_VSYNC_STATE == 0);      //check the Vsync signal		
    while(OV_VSYNC_STATE == 1);      //check the Vsync signal		
    for(m=0; m<160; m++)
    {     
      while(OV_HREF_STATE == 0 );     //HREF High for data valid
      for(n=0; n<128; n++)
      {
        while(OV_PCLK_STATE != 0 );
        while(OV_PCLK_STATE == 0 ); 
        camera_dada[k++] = GPIOB->IDR;   //PCLK high data valid;
        //GPIOB->ODR |= 0x0100;       
        while(OV_PCLK_STATE != 0 ); 
        while(OV_PCLK_STATE == 0 ); 
        camera_dada[k++] = GPIOB->IDR;
        //GPIOB->ODR &= 0xFEFF;   
      }
      while(OV_HREF_STATE != 0 );     //HREF High for data valid
    }
    
 /*display buffer data on TFT;*/
    k=0;
    uint16_t p_temp=0;
    LCD_SetCursor(0,0);
    LCD_WriteRAM_Prepare();	
    for(i=0; i<160; i++)
    {	
      for(j=0; j<128; j++)
      {        
        p_data =  camera_dada[k++]<<8;
        p_data |= camera_dada[k++];
        p_temp  = p_data>>11|p_data<<11|p_data&0x07E0;// BGR  to RGB;
        LCD_WR_DATA(p_temp);                       //TFT GRAM          
      }	  
      LCD_SetCursor(0,i);
      LCD_WriteRAM_Prepare();
    }		
  }  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 20;
  RCC_OscInitStruct.PLL.PLLN = 228;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 5;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* PVD_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(PVD_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(PVD_IRQn);
  /* FLASH_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(FLASH_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(FLASH_IRQn);
  /* RCC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(RCC_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(RCC_IRQn);
  /* FPU_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(FPU_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(FPU_IRQn);
  /* TIM1_BRK_TIM9_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_BRK_TIM9_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(TIM1_BRK_TIM9_IRQn);
  /* TIM1_UP_TIM10_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(TIM1_UP_TIM10_IRQn);
  /* TIM1_TRG_COM_TIM11_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM11_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(TIM1_TRG_COM_TIM11_IRQn);
  /* TIM1_CC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_CC_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(TIM1_CC_IRQn);
  /* SPI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
  HAL_NVIC_DisableIRQ(SPI1_IRQn);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
