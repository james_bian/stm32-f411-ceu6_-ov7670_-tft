/*******************************************************************************
*  Copyright (C) 2012 All Rights Reserved
*		
**********************************************************************************************************************************************/
#include "OV7670.h"
#include "delay.h"
#include "main.h"
#include "tim.h"

//功能：提供7670时钟
//返回：无
void CLK_init_ON(void)
{
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	//TIM1->CCER |= TIM_CCER_CC1E;
}
void CLK_init_OFF(void)
{
	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);	
	//TIM1->CCER &= ~(TIM_CCER_CC1E);
}

void Init_XCLK(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_DOWN;
  htim1.Init.Period = 30;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 15;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);
}

void OV7670_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOB_CLK_ENABLE();
  /*Configure GPIO pins : PB0-7 */
  //GPIO_InitStruct.Pin =  OV7670_D0|OV7670_D1|OV7670_D2|OV7670_D3|OV7670_D4|OV7670_D5|OV7670_D6|OV7670_D7;
  GPIO_InitStruct.Pin =  GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}
void OV7670_GPIO_CONTRL_CONFIG(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOA_CLK_ENABLE();
  /*Configure GPIO pins : PAx */
  GPIO_InitStruct.Pin = OV_HREF_BIT|OV_VSYNC_BIT|OV_PCLK_BIT;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}
////////////////////////////
//功能：写OV7670寄存器
//返回：1-成功	0-失败
unsigned char wrOV7670Reg(unsigned char regID, unsigned char regDat)
{
	startSCCB();
	if(0==SCCBwriteByte(0x42))
	{
		stopSCCB();
		return(0);
	}
	delay_us(100);
  	if(0==SCCBwriteByte(regID))
	{
		stopSCCB();
		return(0);
	}
	delay_us(100);
  	if(0==SCCBwriteByte(regDat))
	{
		stopSCCB();
		return(0);
	}
  	stopSCCB();
	
  	return(1);
}
////////////////////////////
//功能：读OV7670寄存器
//返回：1-成功	0-失败
unsigned char rdOV7670Reg(unsigned char regID, unsigned char *regDat)
{
	//通过写操作设置寄存器地址
	startSCCB();
	if(0==SCCBwriteByte(0x42))
	{
		stopSCCB();
		return(0);
	}
	delay_us(100);
  	if(0==SCCBwriteByte(regID))
	{
		stopSCCB();
		return(0);
	}
	stopSCCB();
	
	delay_us(100);
	
	//设置寄存器地址后，才是读
	startSCCB();
	if(0==SCCBwriteByte(0x43))
	{
		stopSCCB();
		return(0);
	}
	delay_us(100);
  	*regDat=SCCBreadByte();
  	noAck();
  	stopSCCB();
  	return(1);
}


//(140,16,640,480) is good for VGA
//(272,16,320,240) is good for QVGA
/* config_OV7670_window */
void OV7670_config_window(unsigned int startx,unsigned int starty,unsigned int width, unsigned int height)
{
	unsigned int endx;
	unsigned int endy;// "v*2"必须
	unsigned char temp_reg1, temp_reg2;
	unsigned char temp=0;
	
	endx=(startx+width*2);
	endy=(starty+height*2);// "v*2"必须
  rdOV7670Reg(0x03, &temp_reg1 );
	temp_reg1 &= 0xf0;
	rdOV7670Reg(0x32, &temp_reg2 );
	temp_reg2 &= 0xc0;
	
	// Horizontal
	temp = temp_reg2|((endx&0x7)<<3)|(startx&0x7);
	wrOV7670Reg(0x32, temp );
	temp = (startx&0x7F8)>>3;
	wrOV7670Reg(0x17, temp );
	temp = (endx&0x7F8)>>3;
	wrOV7670Reg(0x18, temp );
	
	// Vertical
	temp =temp_reg1|((endy&0x3)<<2)|(starty&0x3);
	wrOV7670Reg(0x03, temp );
	temp = starty>>2;
	wrOV7670Reg(0x19, temp );
	temp = endy>>2;
	wrOV7670Reg(0x1A, temp );
}

void set_OV7670reg(void)
{
	wrOV7670Reg(0x8c, 0x00);
	wrOV7670Reg(0x3a, 0x04);
	wrOV7670Reg(0x40, 0xd0);   //RGB 565 format
	wrOV7670Reg(0x8c, 0x00);
	wrOV7670Reg(0x12, 0x14);   //GVGA, RGB output;
	wrOV7670Reg(0x32, 0x80);
	wrOV7670Reg(0x17, 0x16);
	wrOV7670Reg(0x18, 0x04);
	wrOV7670Reg(0x19, 0x02);
	wrOV7670Reg(0x1a, 0x7b);
  wrOV7670Reg(0x1b, 0x04);
	wrOV7670Reg(0x03, 0x06);
	wrOV7670Reg(0x0c, 0x04);
	wrOV7670Reg(0x3e, 0x00);
	wrOV7670Reg(0x70, 0x3a);
	wrOV7670Reg(0x71, 0x35); 
	wrOV7670Reg(0x72, 0x11); 
	wrOV7670Reg(0x73, 0xf0);
	wrOV7670Reg(0xa2, 0x00);
	wrOV7670Reg(0x11, 0x01);    //internal clock pre-scale, xlk/Bit[5:0]+1

	wrOV7670Reg(0x7a, 0x20); 
	wrOV7670Reg(0x7b, 0x1c); 
	wrOV7670Reg(0x7c, 0x28); 
	wrOV7670Reg(0x7d, 0x3c);
	wrOV7670Reg(0x7e, 0x55); 
	wrOV7670Reg(0x7f, 0x68); 
	wrOV7670Reg(0x80, 0x76);
	wrOV7670Reg(0x81, 0x80); 
	wrOV7670Reg(0x82, 0x88);
	wrOV7670Reg(0x83, 0x8f);
	wrOV7670Reg(0x84, 0x96); 
	wrOV7670Reg(0x85, 0xa3);
	wrOV7670Reg(0x86, 0xaf);
	wrOV7670Reg(0x87, 0xc4); 
	wrOV7670Reg(0x88, 0xd7);
	wrOV7670Reg(0x89, 0xe8);
	 
	wrOV7670Reg(0x32,0xb6);
	
	wrOV7670Reg(0x13, 0xe7); 
	wrOV7670Reg(0x00, 0x00);
	wrOV7670Reg(0x10, 0x00);
	wrOV7670Reg(0x0d, 0x00);
	wrOV7670Reg(0x14, 0x4e);
	wrOV7670Reg(0xa5, 0x05);
	wrOV7670Reg(0xab, 0x07); 
	wrOV7670Reg(0x24, 0x75);
	wrOV7670Reg(0x25, 0x63);
	wrOV7670Reg(0x26, 0xA5);
  wrOV7670Reg(0x2a, 0x00);    //
  wrOV7670Reg(0x2b, 0x00);    //
	wrOV7670Reg(0x9f, 0x78);
	wrOV7670Reg(0xa0, 0x68);
//	wrOV7670Reg(0xa1, 0x03);//0x0b,
	wrOV7670Reg(0xa6, 0xdf);
	wrOV7670Reg(0xa7, 0xdf);
	wrOV7670Reg(0xa8, 0xf0); 
	wrOV7670Reg(0xa9, 0x90); 
	wrOV7670Reg(0xaa, 0x94); 
	//wrOV7670Reg(0x13, 0xe5); 
	wrOV7670Reg(0x0e, 0x61);
	wrOV7670Reg(0x0f, 0x43);
	wrOV7670Reg(0x16, 0x02); 
	wrOV7670Reg(0x1e, 0x37);
	wrOV7670Reg(0x21, 0x02);
	wrOV7670Reg(0x22, 0x91);
	wrOV7670Reg(0x29, 0x07);
	wrOV7670Reg(0x33, 0x0b);
	wrOV7670Reg(0x35, 0x0b);
	wrOV7670Reg(0x37, 0x3f);
	wrOV7670Reg(0x38, 0x01);
	wrOV7670Reg(0x39, 0x00);
	wrOV7670Reg(0x3c, 0x68);
	wrOV7670Reg(0x4d, 0x40);
	wrOV7670Reg(0x4e, 0x20);
	wrOV7670Reg(0x69, 0x00);
	wrOV7670Reg(0x6b, 0x4a);  //4a->0a
	wrOV7670Reg(0x74, 0x19);
	wrOV7670Reg(0x8d, 0x4f);
	wrOV7670Reg(0x8e, 0x00);
	wrOV7670Reg(0x8f, 0x00);
	wrOV7670Reg(0x90, 0x00);
	wrOV7670Reg(0x91, 0x00);
	wrOV7670Reg(0x92, 0x2b);  
  wrOV7670Reg(0x93, 0x00);  
	wrOV7670Reg(0x96, 0x00);
	wrOV7670Reg(0x9a, 0x80);
	wrOV7670Reg(0xb0, 0x84);
	wrOV7670Reg(0xb1, 0x0c);
	wrOV7670Reg(0xb2, 0x0e);
	wrOV7670Reg(0xb3, 0x82);
	wrOV7670Reg(0xb8, 0x0a);
	wrOV7670Reg(0x43, 0x14);
	wrOV7670Reg(0x44, 0xf0);
	wrOV7670Reg(0x45, 0x34);
	wrOV7670Reg(0x46, 0x58);
	wrOV7670Reg(0x47, 0x28);
	wrOV7670Reg(0x48, 0x3a);
	
	wrOV7670Reg(0x59, 0x88);
	wrOV7670Reg(0x5a, 0x88);
	wrOV7670Reg(0x5b, 0x44);
	wrOV7670Reg(0x5c, 0x67);
	wrOV7670Reg(0x5d, 0x49);
	wrOV7670Reg(0x5e, 0x0e);
	
	wrOV7670Reg(0x64, 0x04);
	wrOV7670Reg(0x65, 0x20);
	wrOV7670Reg(0x66, 0x05);

	wrOV7670Reg(0x94, 0x04);
	wrOV7670Reg(0x95, 0x08);	 

	wrOV7670Reg(0x6c, 0x0a);
	wrOV7670Reg(0x6d, 0x55);
	wrOV7670Reg(0x6e, 0x11);
	wrOV7670Reg(0x6f, 0x9f); 

	wrOV7670Reg(0x6a, 0x40);
	wrOV7670Reg(0x01, 0x40);
	wrOV7670Reg(0x02, 0x40);
	
	//wrOV7670Reg(0x13, 0xe7);
	wrOV7670Reg(0x15, 0x00);      //PCLK when HREF is lov
	wrOV7670Reg(0x4f, 0x80);
	wrOV7670Reg(0x50, 0x80);
	wrOV7670Reg(0x51, 0x00);
	wrOV7670Reg(0x52, 0x22);
	wrOV7670Reg(0x53, 0x5e);
	wrOV7670Reg(0x54, 0x80);
    
  wrOV7670Reg(0x55, 0x80);
  wrOV7670Reg(0x56, 0x80);
    
	wrOV7670Reg(0x58, 0x9e);

	wrOV7670Reg(0x41, 0x08);
	wrOV7670Reg(0x3f, 0x00);
	wrOV7670Reg(0x75, 0x05);
	wrOV7670Reg(0x76, 0xe1);

	wrOV7670Reg(0x4c, 0x00);
	wrOV7670Reg(0x77, 0x01);
	
	wrOV7670Reg(0x3d, 0xc2);
	wrOV7670Reg(0x4b, 0x09);
	wrOV7670Reg(0xc9, 0x60);
	//wrOV7670Reg(0x41, 0x38);	
	wrOV7670Reg(0x56, 0x40);
	wrOV7670Reg(0x34, 0x11);
	wrOV7670Reg(0xa4, 0x89);
	
	wrOV7670Reg(0x96, 0x00);
	wrOV7670Reg(0x97, 0x30);
	wrOV7670Reg(0x98, 0x20);
	wrOV7670Reg(0x99, 0x30);
	wrOV7670Reg(0x9a, 0x84);
	wrOV7670Reg(0x9b, 0x29);
	wrOV7670Reg(0x9c, 0x03);
	wrOV7670Reg(0x9d, 0x4c);
	wrOV7670Reg(0x9e, 0x3f);	

	wrOV7670Reg(0x09, 0x00);
	wrOV7670Reg(0x3b, 0x0a);

}

/* OV7670_init() */
//返回1成功，返回0失败
unsigned char OV7670_init(void)
{
	unsigned char temp;	

	OV7670_GPIO_Init();
	OV7670_GPIO_CONTRL_CONFIG();
	SCCB_GPIO_Config(); // io init..

  //CLK_init_ON();
	temp=0x80;
	if(0==wrOV7670Reg(0x12, temp)) //Reset SCCB
	{
    return 0 ;
	}
	delay_ms(100);
  set_OV7670reg();

	OV7670_config_window(400,100,128,160);// set 128*160
	return 0x01; //ok			  
} 





